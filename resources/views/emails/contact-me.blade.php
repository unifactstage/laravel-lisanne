@component('mail::message')

# A heading

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias dolores ea ex excepturi inventore iusto neque nisi non officiis optio, quae quasi soluta velit, veritatis vitae. Architecto ex quo repellat.

- A list
- goes
- Here

Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci assumenda dolor dolore doloribus dolorum hic, impedit ipsa ipsum laboriosam natus nisi, quam quidem repellat rerum suscipit tempore totam voluptate. Nisi.

@component('mail::button', ['url' => 'https://laracasts.com'])
    Visit Laracasts
@endcomponent

@endcomponent
