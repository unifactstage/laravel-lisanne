<?php

namespace App\Http\Controllers;

use App\Events\ProductPurchased;
use App\Notifications\PaymentRecieved;

class PaymentsController extends Controller
{

    public function create()
    {
        return view('payments.create');
    }

    public function store()
    {
        // process the payment
        // unlock the purchase

        ProductPurchased::dispatch('toy');


        // listeners
        // notify the user about the payment
        // award achievements
        // send shareable coupon to user

        request()->user()->notify(new PaymentRecieved(900));
    }
}
