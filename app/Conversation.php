<?php


namespace App;

use Illuminate\Database\Eloquent\Model;


class Conversation extends model
{
    protected $guarded = [];

    public function setBestReply(Reply $reply)
    {
        $this->best_reply_id = $reply->id;
        $this->save();
    }

    public function path()
    {
        return route('conversations.show', $this);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

}
