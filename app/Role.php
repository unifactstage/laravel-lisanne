<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Role extends Model
{
    protected $guarded = [];

    public function abilities()
    {
        return $this->belongsToMany(Ability::class)->withTimestamps();
    }

    public function allowTo($ability)
    {
        if(is_string($ability)){
            $ability = Role::whereName($ability)->firstOrFail();
        }

        $this->abilities()->sync($ability, false);
    }
}
